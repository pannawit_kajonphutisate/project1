﻿using System;
using UnityEngine;
public class Damaged : MonoBehaviour
{
    public int health = 1;
    public float invulnPeriod = 0;
    float invulnTimer = 0;
    int correctLayer;
     
    
    [SerializeField] private AudioClip destroySelf;
    [SerializeField] public float destroySelfVolume = 0.1f;

    SpriteRenderer spriteRend;

    void Start() {
        correctLayer = gameObject.layer;
        spriteRend = GetComponent<SpriteRenderer>();

        if(spriteRend == null) {
            spriteRend = transform.GetComponentInChildren<SpriteRenderer>();

            if(spriteRend==null) {
                Debug.LogError("Object '"+gameObject.name+"' has no sprite renderer.");
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
     {
         Debug.Log("Trigger!");
         
             health--;
             invulnTimer = invulnPeriod;
             gameObject.layer = 10;
         
     }

      void Update()
      {
          if (invulnTimer > 0)
          {
              invulnTimer -= Time.deltaTime;
              if (invulnTimer <= 0)
              {
                  gameObject.layer = correctLayer;
                  if (spriteRend != null)
                  {
                      spriteRend.enabled = true;
                  }
              }
              else
              {
                  if (spriteRend != null)
                  {
                      spriteRend.enabled = !spriteRend.enabled;
                  }
              }
          }
         if(health <= 0)
         {
             Die();
         }
     }
    void Die()
   {
       AudioSource.PlayClipAtPoint(destroySelf,Camera.main.transform.position);
       Destroy(gameObject);
   }
}
